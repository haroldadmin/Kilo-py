# ~~~IMPORTS~~~

import atexit
import curses.ascii
import sys
import termios


# ~~~DATA~~~

#Storing the number that describes STDIN in a variable fd.
fd = sys.stdin.fileno()

# Storing original config as a backup
# Return a list containing the tty attributes for file descriptor fd, as follows: [iflag, oflag, cflag, lflag, ispeed, ospeed, cc]
original_attributes = termios.tcgetattr(fd)

# ~~~TERMINAL~~~

def enable_raw_mode(fd):
	'''This function enables raw mode in the terminal by modifying the lflag attribute.
	It uses the bitwise and operator with the ECHO flag to flip the required bits.'''
	new_attributes = termios.tcgetattr(fd)
	new_attributes[0] = new_attributes[0] & ~(termios.BRKINT | termios.ICRNL | termios.INPCK | termios.ISTRIP | termios.IXON)
	new_attributes[1] = new_attributes[1] & ~(termios.OPOST)
	new_attributes[2] = new_attributes[2] & ~(termios.CS8)
	new_attributes[3] = new_attributes[3] & ~(termios.ECHO | termios.ICANON | termios.IEXTEN | termios.ISIG)
	new_attributes[6][termios.VMIN] = 0
	# new_attributes[6][termios.VTIME] = 1
	termios.tcsetattr(fd, termios.TCSAFLUSH, new_attributes)

def disable_raw_mode(fd):
	'''This function disables raw mode in the terminal by restoring the original attributes
	which were stored previously.'''
	termios.tcsetattr(fd, termios.TCSAFLUSH, original_attributes)


# ~~~INIT~~~

def main():

	enable_raw_mode(fd)
	c = ' '
	while(c != '' and c != 'q'):
		c = sys.stdin.read(1)
		if curses.ascii.iscntrl(c):
			print (ord(c), "\r\n")
		else:
			print (ord(c), c, "\r\n")
	return 0

main()
atexit.register(disable_raw_mode, fd)